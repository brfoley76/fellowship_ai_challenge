
# Part 0: This is a solution for the Language Detection Problem. 

This assumes you have the following, in the working directory:

*    ``./language_classifier.ipynb`` the current jupyter notebook.

*    ``./txt/*`` the European Parliament Proceedings Parallel Corpus, found at <http://www.statmt.org/europarl/>

*    ``./europarl.test`` <https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/language-detection/europarl-test.zip> tab delimited text file with each line comprising 

    * the two-character language code
    
    * a corresponding text fragment
    
This solution tackles the problem by:

*    Pt 1: examining the test data

*    Pt 2: subsetting and formatting the training data to match the test data

*    Pt 3: training and evaluating the classifier (using a Naive Bayes classifier on a bag-of-words tokenization)

*    Pt 4: applying the classifier to the test set

It achieves an accuracy of approximately 0.9992 on the supplied test set, as written.


```python
import numpy as np
import pandas as pd
import scipy as sp
import os
import glob
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import Perceptron 
from sklearn.pipeline import Pipeline
from sklearn.datasets import load_files
from sklearn import metrics
import matplotlib.pyplot as plt
```

# Pt 1: Look at the test data provided.

Read in the test data, and characterise it in terms of word number and character count.



```python
#make a dictionary of word count and another of character count
TestInputPath='./europarl.test'

lineWordCount={}
lineCharCount={}
lineEndChar={}
lineStartChar={}
test=open(TestInputPath, 'r')
for line in test:
    lineNChar=len(line)
    lineWords=line.split()
    lineLanguage=lineWords.pop(0)
    if lineLanguage in lineWordCount:
        lineWordCount[lineLanguage].append(len(lineWords))
        lineCharCount[lineLanguage].append(lineNChar)
    else:
        lineWordCount[lineLanguage]= [len(lineWords)]
        lineCharCount[lineLanguage]= [lineNChar]
```


```python
#examine the quartiles, by language
for key in lineWordCount:
    print key
    print pd.Series(lineWordCount[key]).quantile([0.1,0.5,0.9])
    
for key in lineCharCount:
    print key
    print pd.Series(lineCharCount[key]).quantile([0.05,0.5,0.95])
    
```

*   Examine boxplots of wordcounts-by-language (log transformed to somewhat-normalize the distribution)
   *  I'd usually do glm in R to double-check to see if the test data is good, for the various languages 
   *  It's a pain doing these kinds of tests in Python, frankly, on categorical data.
   *  Eyeballing it, everything looks pretty consistent across languages, and nothing screams 'outlier'.
   *  I'll assume (for now) I don't need to clean anything up.


```python
listOfLists=[]
for key in lineWordCount:
    for element in lineWordCount[key]:
        listOfLists.append([key, element, np.log(element)])
dfWordCount=pd.DataFrame.from_records(listOfLists, columns=['lang', 'words', 'logWords'])        


dfWordCount.boxplot(['logWords'],by='lang')
plt.show()

dfWordCount.boxplot(['words'],by='lang')
plt.show()
```

# Pt. 2: Clean up the training data

*   The test data (above) looks pretty consistent across languages, whether counting chars or words. 

*   I'll take an approximate minimum-word-number per training datapoint of 8 words (max of 80?)

    *  This probably doesn't matter, for most of the machine learning methods you'd use for this particular problem
    
*   I'll discard lines that have the following  pattern : ``<text>``

*   I'll make a new directory called 'parsed_txt' containing the new, cleaned up, data (preserving the file structure)

*   To start with, I'll only save 1 in 40 sentences in the cleaned up data. It'll save time, and likely will be sufficient.

    *  the variable ``samplingDepth`` defines how much of the data I'll keep (``1/samplingDepth``)


```python
outFolder='parsed_txt_mod'
samplingDepth=40 #save 1/samplingDepth sentences from the training data.

if not os.path.exists('./'+outFolder):
    os.mkdir('./'+outFolder)
fileList=glob.glob('./txt/*/*.txt')
tick=0
for x in fileList:
    lan=x.split('/')[2]
    outPath='./'+outFolder+'/'+lan
    if not os.path.exists(outPath):
        os.mkdir(outPath)
    dat=open(x, 'r')
    for line in dat:
        if not line[0]=='<':
            splitLine=line.split('. | ! | ?')
            for sentence in splitLine:
                lengthSentence=len(sentence.split())
                if lengthSentence > 7 and lengthSentence<81:
                    tick=tick+1
                    if tick % samplingDepth == 0 :
                        outName=outPath+'/'+lan+'_'+str(tick)+'.txt'
                        outFile=open(outName, 'w')
                        outFile.write(sentence)
                        outFile.close()                    
    dat.close()
   
```

*   In this case, bag-of-words representations of the corpera are likely to be sufficient to distinguish the language of the texts. 

  * Word n-grams of length > 1 shouldn't add any value, since European languages have pretty different vocabularies. 

  * Character n-grams of length 2-3 might be more efficient than whole words, and less sensitive to vocabulary differences between corpera?

# Pt 3: train the model, and check that it's working


```python
#load the cleaned up data
txt_data_folder = './parsed_txt_mod/'
dataset = load_files(txt_data_folder, shuffle = True)
```


```python
'''
print dir(dataset)
print dataset.data[1:10]
print dataset.target[1:10]
print dataset.target_names
'''
```




    '\nprint dir(dataset)\nprint dataset.data[1:10]\nprint dataset.target[1:10]\nprint dataset.target_names\n'




```python
#split data into test
lang_train, lang_test, y_train, y_test = train_test_split(dataset.data, dataset.target, test_size = 0.2)
```


```python
# do a brute force dictionary of unique words
from sklearn.feature_extraction.text import CountVectorizer
#count_vect = CountVectorizer()
#X_train_counts = count_vect.fit_transform(lang_train)
#X_train_counts.shape
# naive bayes seems like the best approach for a CountVectorized dataset
from sklearn.naive_bayes import MultinomialNB
#NaiveClass = MultinomialNB().fit(X_train_counts,y_train)
```


```python
textNaiveClass = Pipeline([('vect', CountVectorizer()),
                     ('clf', MultinomialNB()),
                    ])

textNaiveClass = textNaiveClass.fit(lang_train, y_train)
```


```python
predicted=textNaiveClass.predict(lang_test)
```


```python
#overall proportion of correct calls
nRight=(predicted != y_test)
np.mean(nRight)
```




    0.00074494831921035481




```python
print metrics.confusion_matrix(y_test, predicted)
```

    [[ 489    0    0    0    1    0    0    0    0    0    0    0    0    0
         0    1    0    0    0    0    0]
     [   0  877    0    0    0    0    0    0    0    0    0    0    0    0
         0    0    0    0    0    0    0]
     [   0    0 2295    0    0    0    0    0    0    0    0    0    0    0
         0    1    0    0    0    0    0]
     [   0    0    0 2294    0    1    0    0    0    0    0    0    0    0
         0    0    0    0    0    0    0]
     [   0    0    0    0 1552    0    0    0    0    0    0    0    0    0
         0    0    0    0    0    0    0]
     [   0    0    0    0    0 2272    0    0    0    0    0    0    0    0
         0    0    0    0    0    0    0]
     [   0    0    0    0    0    1 2088    0    0    0    0    0    0    0
         0    0    0    0    0    0    0]
     [   0    0    0    0    0    0    0  933    0    0    0    0    0    0
         0    0    0    0    0    0    0]
     [   0    0    0    0    0    2    0    0 2791    0    0    0    0    0
         0    0    0    0    0    0    0]
     [   0    0    0    0    0    0    0    0    0 2141    0    0    0    0
         0    0    0    0    0    0    0]
     [   0    0    0    0    0    0    0    0    1    1  855    1    0    0
         0    0    0    0    0    0    0]
     [   0    0    0    0    0    0    0    0    2    0    0 2242    0    0
         0    0    0    0    0    0    0]
     [   0    0    0    0    0    0    0    0    1    2    0    0  909    0
         0    0    0    0    0    0    0]
     [   0    0    0    0    0    0    0    0    0    1    0    1    0  910
         0    0    0    0    0    0    0]
     [   0    0    0    0    0    0    0    0    1    0    0    0    0    0
      2126    0    0    0    0    0    0]
     [   0    0    0    0    0    0    0    0    0    0    0    0    0    0
         0  863    0    0    0    0    0]
     [   0    0    0    0    0    1    0    0    0    0    0    0    0    0
         0    0 2105    0    0    0    0]
     [   0    0    0    0    0    0    0    0    0    0    0    0    0    0
         0    1    0  464    0    0    0]
     [   0    0    0    0    0    0    0    0    0    3    0    0    0    0
         0    0    0    0  855    0    0]
     [   0    0    0    0    0    0    0    0    0    0    0    0    0    0
         0    0    0    0    0  864    0]
     [   0    0    0    0    0    1    0    0    0    0    0    0    0    0
         0    0    0    0    0    0 2268]]



```python
print metrics.classification_report(y_test, predicted,
                                    target_names=dataset.target_names)
```

                 precision    recall  f1-score   support
    
             bg       1.00      1.00      1.00       491
             cs       1.00      1.00      1.00       877
             da       1.00      1.00      1.00      2296
             de       1.00      1.00      1.00      2295
             el       1.00      1.00      1.00      1552
             en       1.00      1.00      1.00      2272
             es       1.00      1.00      1.00      2089
             et       1.00      1.00      1.00       933
             fi       1.00      1.00      1.00      2793
             fr       1.00      1.00      1.00      2141
             hu       1.00      1.00      1.00       858
             it       1.00      1.00      1.00      2244
             lt       1.00      1.00      1.00       912
             lv       1.00      1.00      1.00       912
             nl       1.00      1.00      1.00      2127
             pl       1.00      1.00      1.00       863
             pt       1.00      1.00      1.00      2106
             ro       1.00      1.00      1.00       465
             sk       1.00      1.00      1.00       858
             sl       1.00      1.00      1.00       864
             sv       1.00      1.00      1.00      2269
    
    avg / total       1.00      1.00      1.00     32217
    


# Pt. 4: load in the test data, and see if the trained model works. 

*   The model above looks pretty perfect. Let's see if it works on the target test set.


```python
dfTestData=pd.read_table(TestInputPath, sep='\t', header=None, names=['lang','text'])
dfTestData.iloc[:5]
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>lang</th>
      <th>text</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>bg</td>
      <td>Европа 2020 не трябва да стартира нов конкурен...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>bg</td>
      <td>(CS) Най-голямата несправедливост на сегашната...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>bg</td>
      <td>(DE) Г-жо председател, г-н член на Комисията, ...</td>
    </tr>
    <tr>
      <th>3</th>
      <td>bg</td>
      <td>(DE) Г-н председател, бих искал да започна с к...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>bg</td>
      <td>(DE) Г-н председател, въпросът за правата на ч...</td>
    </tr>
  </tbody>
</table>
</div>




```python
from sklearn.datasets.base import Bunch

test_set=Bunch(data=list(dfTestData['text']), target=pd.Series(dfTestData['lang']).values, target_names=dataset.target_names)
tick=0
for val in test_set.target_names:
    ccc=test_set.target[:]==val
    test_set.target[ccc]=tick
    tick=tick+1
```


```python
predictedFinal=textNaiveClass.predict(test_set.data)
```


```python
nRight=(predictedFinal != test_set.target)
np.mean(nRight)
```




    0.00081620894949106966


