#This is a solution for the Language Detection Problem. 

This assumes you have the following, in the working directory:

*    ``./language_classifier.ipynb`` the current jupyter notebook.

*    ``./txt/*`` the European Parliament Proceedings Parallel Corpus, found at <http://www.statmt.org/europarl/>

*    ``./europarl.test`` <https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/language-detection/europarl-test.zip> tab delimited text file with each line comprising 

    * the two-character language code
    
    * a corresponding text fragment
    
This solution tackles the problem by:

*    Pt 1: examining the test data

*    Pt 2: subsetting and formatting the training data to match the test data

*    Pt 3: training and evaluating the classifier (using a Naive Bayes classifier on a bag-of-words tokenization)

*    Pt 4: applying the classifier to the test set

It achieves an accuracy of approximately 0.9992 on the supplied test set, as written.

# It depends on:

*   Jupyter Notebook running python2.7 

*   numpy, scipy, pandas, scikit_learn

